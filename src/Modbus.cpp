#include "Modbus.hpp"

#include "conf_usart.h"
#include "hwlib.hpp"
#include "usart.h"

Modbus::Modbus() {}

bool Modbus::sendMessage(uint8_t functionCode, bool data[], uint8_t slaveAddres) {
    usart_putchar(USART_SERIAL, functionCode);
    usart_putchar(USART_SERIAL, functionCode);
    usart_putchar(USART_SERIAL, 0x01);

    uint16_t errorCheck = calcErrorCheck(slaveAddres);
    usart_putchar(USART_SERIAL, errorCheck >> 8 & 0xFF);

    hwlib::wait_ms(14); // process time
    // read
    hwlib::wait_ms(14); // process time
    return false;
}

uint16_t Modbus::calcErrorCheck(uint8_t slaveAddres) {
    uint8_t result = 0XFFFF ^ slaveAddres;
    uint8_t firstShift = result >> 1;

    result = 0xA001 ^ firstShift;
    uint8_t secondShift = result >> 1;

    result = 0xA001 ^ secondShift;
    uint8_t thirdShift = result >> 1;
    uint8_t forthShift = thirdShift >> 1;

    result = 0xA001 ^ forthShift;
    uint8_t fifthShift = result >> 1;
    uint8_t sixthShift = fifthShift >> 1;

    result = 0xA001 ^ sixthShift;
    uint8_t seventhShift = result >> 1;
    uint8_t eighthShift = seventhShift >> 1;

    result = 0xA001 ^ eighthShift;
    /////////////////////////////////////////////////
    result = 0x07 ^ eighthShift;
    firstShift = result >> 1;

    result = 0xA001 ^ firstShift;
    secondShift = result >> 1;

    result = 0xA001 ^ secondShift;
    thirdShift = result >> 1;

    result = 0xA001 ^ thirdShift;
    forthShift = result >> 1;
    fifthShift = forthShift >> 1;

    result = 0xA001 ^ fifthShift;
    sixthShift = result >> 1;
    seventhShift = sixthShift >> 1;
    eighthShift = seventhShift >> 1;

    return eighthShift;
}