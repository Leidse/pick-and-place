

#include "CRD514KD.hpp"
#include "conf_usart.h"
#include "memory.h"
#include "sysclk.h"
#include "usart.h"
#include "Modbus.hpp"

int main() {
    SystemInit();

    sysclk_init();

    const sam_usart_opt_t usart_console_settings = {USART_SERIAL_BAUDRATE, USART_SERIAL_CHAR_LENGTH, USART_SERIAL_PARITY,
                                                    USART_SERIAL_STOP_BIT, US_MR_CHMODE_NORMAL};
    sysclk_enable_peripheral_clock(USART_SERIAL_ID);
    usart_enable_tx(USART_SERIAL);
    usart_enable_rx(USART_SERIAL);

    Modbus modbus = Modbus();

    CRD514KD Crd = CRD514KD(modbus);
    Crd.setAwo(1);

    return 0;
}