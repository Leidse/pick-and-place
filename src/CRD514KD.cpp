#include "CRD514KD.hpp"

#include "Modbus.hpp"

CRD514KD::CRD514KD(Modbus& modbus) : modbus(modbus) {}

CRD514KD::~CRD514KD(){};

/**
 * This function sets the awo register to enable of disable torque.
 *
 **/
bool CRD514KD::setAwo(bool enabled) {
    uint8_t functionCode = 0x1E;
    bool data[8] = {0, 0, 0, 0, 0, 0, 0, enabled};
    auto ret = modbus.sendMessage(functionCode, data);
    return ret;
}