#include "stdint.h" 

class Modbus {
private:
public:
Modbus();

bool sendMessage(uint8_t functionCodeByte, bool data[], uint8_t slaveAddresByte = 0);
uint16_t calcErrorCheck(uint8_t slaveAddres);
}
;