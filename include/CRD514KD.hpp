#include <memory>

class Modbus;

class CRD514KD {
  private:
    Modbus& modbus;

  public:
    CRD514KD(Modbus& modbus);
    ~CRD514KD();

    bool setAwo(bool enabled);
};
